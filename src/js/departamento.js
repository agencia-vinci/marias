$(document).ready(function() {
    $('.bread-crumb ul li').first().html('<a href="/">Início</a>');
});

if (!String.prototype.slugify) {
    String.prototype.slugify = function() {

        return this.toString().toLowerCase()
            .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
            .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
            .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
            .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
            .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
            .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
            .replace(/[ñÑ]+/g, 'n') // Special Characters #7
            .replace(/[çÇ]+/g, 'c') // Special Characters #8
            .replace(/[ß]+/g, 'ss') // Special Characters #9
            .replace(/[Ææ]+/g, 'ae') // Special Characters #10
            .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
            .replace(/[%]+/g, 'pct') // Special Characters #12
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[\/"]/g, '-') // Trim - from end of text
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text



    };
}
setTimeout(() => {
    $('.search-single-navigator a').each(function() {
        let text = $(this).text();
        text = text.split('(', 1)[0];
        $(this).text(text);
    })
}, 1000)



const width = $(document).width();
if (width < 992) {
$('#content').prepend("<span class='toggler-busca'>Filtro</span>");
$('.department-link').prepend("<span class='toggler-busca'>Filtro</span>");


    $('.toggler-busca').click(function() {
        $('.department-link').toggleClass('active')
        $('.box-shadow').fadeToggle();
        $('body').toggleClass('menu-open')
    })
}