/*MARCAS*/
const marcaMenu = document.querySelector('.marcas-vitrine');
if (marcaMenu) {
    const settingsMarca = {
        "url": "/api/catalog_system/pub/brand/list",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        },
    };


    $.ajax(settingsMarca).done(function(response) {


        /** ordenação  ....  Marcas*/
        response.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            // a must be equal to b
            return 0;
        });

        /**  fim ordenação .... Marcas */

        for (response of response) {
            if (response.imageUrl) {
                marcaMenu.insertAdjacentHTML(
                    'beforeend',
                    `<a href="${response.name}" class="marca-link">
                        <img src="/arquivos/ids${response.imageUrl}">
                    </a>`
                );
            }

        


        }



    $('.marcas-vitrine').slick({
            centerMode: true,
            centerPadding: '10px',
            slidesToShow: 4,
            speed: 400,
            autoplay: true,
            responsive: [
                {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    }

                },
                {
                breakpoint: 568,
                settings: {
                    slidesToShow: 1,
                    }

                }
            ]
        });
    });

}

window.onload = function() {
    const screenWidth = window.innerWidth;

    $('.helperComplement').remove()
        //BANNER
    const $fullbanner = $(".main-banner > article");
    if ($fullbanner.length) {
        $fullbanner.owlCarousel({
            items: 1,
            loop: true,
            singleItem: true,
            autoPlay: true,
            stopOnHover: true,
            navigation: true,
            autoHeight: true,
            navigationText: ['<div class="arrow-box-left"><i class="arrow-left"></i></div>', '<div class="arrow-box-right"><i class="arrow-right"></i></div>'],
        });
    }

    //THIS FUNCTIONS LOAD OWL IN ALL VITRINES IN HOME PAGE AND DO VALIDATIONS
    $('.js-vitrine').each(function(vitrine) {
        //VALIDATION TO CHECK IF EXISTS ITEMS ARE IN VITRINE
        const child = $(this).find('.vitrine ul')

        if (!child) {
            $(child).remove()
            return
        }
        //take text to title
        $(this).find('.title-vitrine p').text($(this).find('.vitrine h2').text())
        $(this).find('.vitrine h2').remove()

        //start vitrine
        $(this).find('.vitrine').owlCarousel({
            items: 4,
            autoPlay: true,
            autoHeight: true,
            pagination: false,
            itemsDesktop: [1200, 4],
            itemsDesktopSmall: [991, 3],
            itemsTablet: [767, 2],
            itemsMobile: [567, 1],
            navigation: true,
            stopOnHover: true,
            autoHeight: true,
            navigationText: ['<img src="/arquivos/seta-banner-esquerda.png" class="img-responsive arrow-vitrine" />', '<img src="/arquivos/seta-banner-direita.png" class="img-responsive arrow-vitrine" />'],
        });
    });

    //if (screenWidth < 992) {
        $('.main-icons').slick({
            centerMode: true,
            centerPadding: '10px',
            slidesToShow: 4,
            speed: 400,
            autoplay: true,
            responsive: [
                {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    }

                },
                {
                breakpoint: 568,
                settings: {
                    slidesToShow: 1,
                    }

                }

            ]
        });
    //}



    

}
