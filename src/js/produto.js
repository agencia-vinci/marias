

$(document).ready(function(){
    if($('.thumbs li').length > 4){
        $('.thumbs').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                }
                },
                {
                    breakpoint: 567,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        })
    }


    $('.sku-selector-container').click(initThumbsCarousel)
});

function initThumbsCarousel(){

    $('.thumbs').slick('destroy');
    
    setTimeout(function(){
        $('.thumbs').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                    slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 567,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        })
    },1000)

}

$('.vitrine-acessorios').each(function(vitrine) {
    const child = $(this).find('.vitrine')

    $(this).find('.title-vitrine p').text($(this).find('.vitrine h2').text())
    $(this).find('.vitrine h2').remove()
})

setTimeout(function() {
    $('.product-rating strong').text('Avaliações: ')
    $('input#txtCep').ready(function() {
        $(this).attr('placeholder', 'Digite seu CEP')
    })

    const field = document.querySelector('.bt.freight-btn')
    field.setAttribute('value', 'Calcular')
}, 3000)




$(document).ready(function() {
    $('.bread-crumb ul li').first().html('<a title="TEMA 1" href="/">Página Inicial</a>');
});


$(document).ready(function() {
    $("ul.item-descrip li").click(function(e) {
        e.preventDefault();
        const element = $(this).children("a").attr('href');
        $('ul.item-descrip li').removeClass('item-desc-active');
        $(this).addClass('item-desc-active');
        $('.product-desc-container > div').hide();
        $(element).fadeIn();
    });
});


//THIS FUNCTIONS LOAD OWL IN ALL VITRINES IN HOME PAGE AND DO VALIDATIONS
$('.js-vitrine').each(function(vitrine) {
    //VALIDATION TO CHECK IF EXISTS ITEMS ARE IN VITRINE
    const child = $(this).find('.vitrine ul')

    if (!child) {
        $(child).remove()
        return
    }
    //take text to title
    $(this).find('.title-vitrine p').text($(this).find('.vitrine h2').text())
    $(this).find('.vitrine h2').remove()

    //start vitrine
    $(this).find('.vitrine').owlCarousel({
        items: 4,
        autoPlay: true,
        autoHeight: true,
        pagination: false,
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [991, 3],
        itemsTablet: [767, 2],
        itemsMobile: [567, 1],
        navigation: true,
        stopOnHover: true,
        autoHeight: true,
        navigationText: ['<img src="/arquivos/seta-banner-esquerda.png" class="img-responsive arrow-vitrine" />', '<img src="/arquivos/seta-banner-direita.png" class="img-responsive arrow-vitrine" />'],
    });
});


window.onload = function(){ 
    setTimeout(function(){
        document.querySelector('#txtCep').setAttribute('placeholder','Digite aqui seu CEP');
    }, 2000);
}